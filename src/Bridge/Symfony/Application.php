<?php

declare(strict_types=1);

namespace Weather\Bridge\Symfony;

use Symfony\Component\Console\Application as SymfonyConsoleApplication;

final class Application extends SymfonyConsoleApplication
{
    public function __construct(iterable $commands = [])
    {
        parent::__construct();

        foreach ($commands as $command) {
            $this->add($command);
        }
    }
}
