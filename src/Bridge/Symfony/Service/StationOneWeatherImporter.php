<?php

declare(strict_types=1);

namespace Weather\Bridge\Symfony\Service;

use DateTimeImmutable;
use Psr\Log\LoggerInterface;
use Weather\Bridge\Symfony\Enum\StationEnum;
use Weather\Infrastructure\Enum\Station;
use Weather\Infrastructure\Service\StationWeatherImporter;

class StationOneWeatherImporter implements StationWeatherImporter
{
    public function __construct(
        private LoggerInterface $logger
    ) {
    }

    public function station(): Station
    {
        return StationEnum::STATION_1();
    }

    public function execute(DateTimeImmutable $dateToImport): array
    {
        $this->logger->notice('Station 1 import triggered');

        return [];
    }
}
