<?php

declare(strict_types=1);

namespace Weather\Bridge\Symfony\Service;

use DateTimeImmutable;
use Psr\Log\LoggerInterface;
use Weather\Bridge\Symfony\Enum\StationEnum;
use Weather\Infrastructure\Enum\Station;
use Weather\Infrastructure\Service\StationWeatherImporter;

final class StationThreeWeatherImporter implements StationWeatherImporter
{
    public function __construct(
        private LoggerInterface $logger
    ) {
    }

    public function station(): Station
    {
        return StationEnum::STATION_3();
    }

    public function execute(DateTimeImmutable $dateToImport): array
    {
        $this->logger->notice('Station 3 import triggered');

        return [];
    }
}
