<?php

declare(strict_types=1);

namespace Weather\Bridge\Symfony\Repository;

use DateTimeImmutable;
use Decimal\Decimal;
use Psr\Log\LoggerInterface;
use Weather\Bridge\Symfony\Enum\SpeedScaleEnum;
use Weather\Bridge\Symfony\Enum\TemperatureScaleEnum;
use Weather\Domain\Entity\WeatherLog;
use Weather\Domain\Model\Temperature;
use Weather\Domain\Model\Wind;
use Weather\Infrastructure\Enum\Station;
use Weather\Infrastructure\Repository\WeatherLogRepository;

final class StubWeatherLogRepository implements WeatherLogRepository
{
    public function __construct(
        private LoggerInterface $logger
    ) {
    }

    public function fromStationByTime(Station $station, DateTimeImmutable $timestamp): ?WeatherLog
    {
        $this->logger->debug('fromStationByTime triggered');

        return new WeatherLog(
            $station,
            $timestamp,
            new Temperature(
                new Decimal(random_int(15, 25)),
                TemperatureScaleEnum::CELSIUS()
            ),
            new Decimal('0.5'),
            new Wind(
                new Decimal(random_int(5, 15)),
                SpeedScaleEnum::KMPH()
            )
        );
    }

    public function averageByDate(DateTimeImmutable $timestamp, Station ...$stations): array
    {
        $this->logger->debug('averageByDate triggered');

        return [];
    }

    public function lastImportDate(): ?DateTimeImmutable
    {
        $this->logger->debug('lastImportDate triggered');

        return new DateTimeImmutable('today -1 day');
    }

    public function create(WeatherLog $weather): void
    {
        $this->logger->debug('Received WeatherLog instance to create', [
            'weather_log' => $weather,
        ]);
    }
}
