<?php

declare(strict_types=1);

namespace Weather\Bridge\Symfony\DependencyInjection\Import;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Weather\Domain\Service\WeatherImporter;

final class ImportPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (! $container->has(WeatherImporter::class)) {
            return;
        }

        $definition = $container->findDefinition(WeatherImporter::class);
        $taggedServices = $container->findTaggedServiceIds('app.station_importer');

        foreach ($taggedServices as $id => $service) {
            $definition->addMethodCall('addImporter', [new Reference($id)]);
        }
    }
}
