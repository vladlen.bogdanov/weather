<?php

declare(strict_types=1);

namespace Weather\Bridge\Symfony\Console\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Weather\Application\Service\ReadLatestImportedDateService;

final class ImportLastDateCommand extends Command
{
    protected static $defaultName = 'import:last-date';
    protected static $defaultDescription = 'Shows last import date';

    public function __construct(
        private ReadLatestImportedDateService $latestImportedDateService,
        private LoggerInterface $logger
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $latestImportDate = $this->latestImportedDateService->execute();
        $this->logger->info('import:last-date command triggered', [
            'result' => $latestImportDate,
        ]);

        return 0;
    }
}
