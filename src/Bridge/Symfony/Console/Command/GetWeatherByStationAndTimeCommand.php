<?php

declare(strict_types=1);

namespace Weather\Bridge\Symfony\Console\Command;

use DateTimeImmutable;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Weather\Application\Service\ReadStationWeatherService;
use Weather\Application\Service\ReadStationWeatherServiceContext;
use Weather\Bridge\Symfony\Enum\StationEnum;

final class GetWeatherByStationAndTimeCommand extends Command
{
    protected static $defaultName = 'weather:from-station';
    protected static $defaultDescription = 'Shows weather info from requested station';

    public function __construct(
        private ReadStationWeatherService $stationWeatherService,
        private LoggerInterface $logger
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('station', InputArgument::REQUIRED, 'Station enum value');
        $this->addArgument('datetime', InputArgument::OPTIONAL, 'YYYY-MM-DD HH:ii', 'now');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $context = new ReadStationWeatherServiceContext(
            new StationEnum($input->getArgument('station')),
            new DateTimeImmutable($input->getArgument('datetime'))
        );

        $weather = $this->stationWeatherService->execute($context);

        if (null === $weather) {
            $output->writeln('Not found');

            return 0;
        }

        $output->writeln('Weather info: ' . json_encode([
                'station' => $weather->station()->getValue(),
                'timestamp' => $weather->timestamp()->getTimestamp(),
                'temperature' => [
                    'scale' => $weather->temperature()->scale(),
                    'degree' => $weather->temperature()->degree(),
                ],
                'wind' => [
                    'speed' => $weather->wind()->speed(),
                    'scale' => $weather->wind()->scale(),
                ],
                'humidity' => $weather->humidity()->toFixed(2),
            ]));

        return 0;
    }
}
