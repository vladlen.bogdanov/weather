<?php

declare(strict_types=1);

namespace Weather\Bridge\Symfony\Console\Command;

use DateTimeImmutable;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Weather\Application\Service\ReadStationAverageWeatherService;
use Weather\Application\Service\ReadStationAverageWeatherServiceContext;
use Weather\Bridge\Symfony\Enum\StationEnum;

final class GetAverageWeatherCommand extends Command
{
    protected static $defaultName = 'weather:average';
    protected static $defaultDescription = 'Shows average weather info from requested stations';

    public function __construct(
        private ReadStationAverageWeatherService $stationAverageWeatherService,
        private LoggerInterface $logger
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('date', 'd', InputArgument::OPTIONAL, 'YYYY-MM-DD', 'today');
        $this->addArgument('stations', InputArgument::IS_ARRAY, 'List of stations');
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $stations = $input->getArgument('stations');
        $date = new DateTimeImmutable($input->getOption('date'));
        $serviceContext = new ReadStationAverageWeatherServiceContext($date);

        array_walk(
            $stations,
            static function (string $station) use ($serviceContext): void {
                $serviceContext->addStation(new StationEnum($station));
            }
        );

        $average = $this->stationAverageWeatherService->execute($serviceContext);

        // todo implement output when real data will be provided
        // todo use JMS serializer for that

        $this->logger->warning('weather:average triggered - do not forget to finish this command :)');

        return 0;
    }
}
