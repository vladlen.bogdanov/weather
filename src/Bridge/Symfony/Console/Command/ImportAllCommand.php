<?php

declare(strict_types=1);

namespace Weather\Bridge\Symfony\Console\Command;

use DateTimeImmutable;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Weather\Application\Service\ImportWeatherDataService;

final class ImportAllCommand extends Command
{
    protected static $defaultName = 'import:all';
    protected static $defaultDescription = 'Runs import from all stations';

    public function __construct(
        private ImportWeatherDataService $importWeatherDataService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('import-date', InputArgument::OPTIONAL, 'YYYY-MM-DD', date('Y-m-d'));
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $importDate = DateTimeImmutable::createFromFormat(
            'Y-m-d',
            $input->getArgument('import-date')
        );

        $this->importWeatherDataService->execute($importDate);

        return 0;
    }
}
