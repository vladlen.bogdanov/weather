<?php

declare(strict_types=1);

namespace Weather\Bridge\Symfony;

use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as SymfonyKernel;
use Weather\Bridge\Symfony\DependencyInjection\Import\ImportPass;

final class Kernel extends SymfonyKernel
{
    public function registerBundles(): array
    {
        return [
            new MonologBundle(),
        ];
    }

    public function registerContainerConfiguration(LoaderInterface $loader): void
    {
        $loader->load(ROOT . '/config/app.yml');
    }

    protected function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new ImportPass());
    }
}
