<?php

declare(strict_types=1);

namespace Weather\Bridge\Symfony\Enum;

use Weather\Infrastructure\Enum\SpeedScale;

/**
 * @method static self KMPH();
 * @method static self MPH();
 */
final class SpeedScaleEnum extends SpeedScale
{
    public const KMPH = 'kmph';
    public const MPH = 'mph';
}
