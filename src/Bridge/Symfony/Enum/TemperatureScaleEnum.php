<?php

declare(strict_types=1);

namespace Weather\Bridge\Symfony\Enum;

use Weather\Infrastructure\Enum\TemperatureScale;

/**
 * @method static self CELSIUS();
 * @method static self FAHRENHEIT();
 */
final class TemperatureScaleEnum extends TemperatureScale
{
    public const CELSIUS = 'C';
    public const FAHRENHEIT = 'F';
}
