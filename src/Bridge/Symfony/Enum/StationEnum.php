<?php

declare(strict_types=1);

namespace Weather\Bridge\Symfony\Enum;

use Weather\Infrastructure\Enum\Station;

/**
 * @method static self STATION_1();
 * @method static self STATION_2();
 * @method static self STATION_3();
 */
final class StationEnum extends Station
{
    public const STATION_1 = 'station_1';
    public const STATION_2 = 'station_2';
    public const STATION_3 = 'station_3';
}
