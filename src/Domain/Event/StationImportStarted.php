<?php

declare(strict_types=1);

namespace Weather\Domain\Event;

use DateTimeImmutable;
use Ramsey\Uuid\UuidInterface;

final class StationImportStarted extends StationImportEvent
{
    private DateTimeImmutable $importDate;

    public function __construct(
        UuidInterface $importId,
        DateTimeImmutable $importDate
    ) {
        parent::__construct($importId);
        $this->importDate = $importDate;
    }

    public function importDate(): DateTimeImmutable
    {
        return $this->importDate;
    }
}
