<?php

declare(strict_types=1);

namespace Weather\Domain\Event;

final class StationImportFinished extends StationImportEvent
{
}
