<?php

declare(strict_types=1);

namespace Weather\Domain\Event;

use Ramsey\Uuid\UuidInterface;

abstract class StationImportEvent
{
    public function __construct(
        private UuidInterface $importId
    ) {
    }

    public function importId(): UuidInterface
    {
        return $this->importId;
    }
}
