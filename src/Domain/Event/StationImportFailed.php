<?php

declare(strict_types=1);

namespace Weather\Domain\Event;

final class StationImportFailed extends StationImportEvent
{
}
