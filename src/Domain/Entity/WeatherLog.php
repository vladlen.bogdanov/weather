<?php

declare(strict_types=1);

namespace Weather\Domain\Entity;

use DateTimeImmutable;
use Decimal\Decimal;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Weather\Domain\Model\Temperature;
use Weather\Domain\Model\Wind;
use Weather\Infrastructure\Enum\Station;

// todo: JMS annotations should be added
class WeatherLog
{
    private UuidInterface $id;

    public function __construct(
        private Station $station,
        private DateTimeImmutable $timestamp,
        private Temperature $temperature,
        private Decimal $humidity,
        private Wind $wind
    ) {
        $this->id = Uuid::uuid4();
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function station(): Station
    {
        return $this->station;
    }

    public function timestamp(): DateTimeImmutable
    {
        return $this->timestamp;
    }

    public function temperature(): Temperature
    {
        return $this->temperature;
    }

    public function humidity(): Decimal
    {
        return $this->humidity;
    }

    public function wind(): Wind
    {
        return $this->wind;
    }
}
