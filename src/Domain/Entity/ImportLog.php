<?php

declare(strict_types=1);

namespace Weather\Domain\Entity;

use DateTimeImmutable;
use Ramsey\Uuid\UuidInterface;
use Weather\Infrastructure\Enum\ImportStatus;
use Weather\Infrastructure\Enum\Station;

final class ImportLog
{
    private DateTimeImmutable $startedAt;
    private DateTimeImmutable $updatedAt;
    private ImportStatus $status;

    public function __construct(
        private UuidInterface $id,
        private Station $station,
        private DateTimeImmutable $importDate
    ) {
        $this->startedAt = new DateTimeImmutable();
        $this->status = ImportStatus::STARTED();
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function startedAt(): DateTimeImmutable
    {
        return $this->startedAt;
    }

    public function updatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function status(): ImportStatus
    {
        return $this->status;
    }

    public function importDate(): DateTimeImmutable
    {
        return $this->importDate;
    }

    public function station(): Station
    {
        return $this->station;
    }
}
