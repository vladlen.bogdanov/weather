<?php

declare(strict_types=1);

namespace Weather\Domain\Model;

use Decimal\Decimal;
use Weather\Infrastructure\Enum\TemperatureScale;

final class Temperature
{
    public function __construct(
        protected Decimal $degree,
        protected TemperatureScale $scale
    ) {
    }

    public function degree(): Decimal
    {
        return $this->degree;
    }

    public function scale(): TemperatureScale
    {
        return $this->scale;
    }
}
