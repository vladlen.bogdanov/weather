<?php

declare(strict_types=1);

namespace Weather\Domain\Model;

use DateTimeImmutable;
use Decimal\Decimal;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Weather\Domain\Entity\WeatherLog;
use Weather\Infrastructure\Enum\Station;

final class Weather
{
    private UuidInterface $id;

    public function __construct(
        private Station $station,
        private DateTimeImmutable $timestamp,
        private Temperature $temperature,
        private Decimal $humidity,
        private Wind $wind
    ) {
        $this->id = Uuid::uuid4();
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function station(): Station
    {
        return $this->station;
    }

    public function timestamp(): DateTimeImmutable
    {
        return $this->timestamp;
    }

    public function temperature(): Temperature
    {
        return $this->temperature;
    }

    public function humidity(): Decimal
    {
        return $this->humidity;
    }

    public function wind(): Wind
    {
        return $this->wind;
    }

    public function changeTemperature(Temperature $temperature): void
    {
        $this->temperature = $temperature;
    }

    public function changeWind(Wind $wind): void
    {
        $this->wind = $wind;
    }

    public static function fromEntity(WeatherLog $entity): self
    {
        return new self(
            $entity->station(),
            $entity->timestamp(),
            $entity->temperature(),
            $entity->humidity(),
            $entity->wind()
        );
    }
}
