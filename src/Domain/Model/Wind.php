<?php

declare(strict_types=1);

namespace Weather\Domain\Model;

use Decimal\Decimal;
use Weather\Infrastructure\Enum\SpeedScale;

final class Wind
{
    public function __construct(
        protected Decimal $speed,
        protected SpeedScale $scale
    ) {
    }

    public function speed(): Decimal
    {
        return $this->speed;
    }

    public function scale(): SpeedScale
    {
        return $this->scale;
    }
}
