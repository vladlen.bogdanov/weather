<?php

declare(strict_types=1);

namespace Weather\Domain\Service;

use DateTimeImmutable;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Throwable;
use Weather\Domain\Event\StationImportFailed;
use Weather\Domain\Event\StationImportFinished;
use Weather\Domain\Event\StationImportStarted;
use Weather\Infrastructure\Repository\WeatherLogRepository;
use Weather\Infrastructure\Service\StationWeatherImporter;

final class WeatherImporter
{
    /* @var StationWeatherImporter[] */
    private array $importers = [];

    public function __construct(
        private WeatherLogRepository $weatherLogRepository,
        private EventDispatcherInterface $eventDispatcher,
        private LoggerInterface $logger,
        private array $enabledStations
    ) {
    }

    public function addImporter(StationWeatherImporter $importer): void
    {
        if (! in_array($importer->station()->getValue(), $this->enabledStations, true)) {
            return;
        }

        $this->importers[] = $importer;
    }

    public function execute(DateTimeImmutable $importDate): void
    {
        foreach ($this->importers as $importer) {
            $importId = Uuid::uuid4();

            $this->eventDispatcher->dispatch(
                new StationImportStarted($importId, $importDate)
            );

            try {
                // todo: move to sync for better performance
                $weatherLogRecords = $importer->execute($importDate);

                foreach ($weatherLogRecords as $record) {
                    $this->weatherLogRepository->create($record);
                }
            } catch (Throwable $exception) {
                $this->eventDispatcher->dispatch(
                    new StationImportFailed($importId)
                );

                $this->logger->error('Station import failed', [
                    'import_id' => $importId,
                    'message' => $exception->getMessage(),
                    'trace' => $exception->getTraceAsString(),
                ]);

                continue;
            }

            $this->eventDispatcher->dispatch(
                new StationImportFinished($importId)
            );
        }
    }
}
