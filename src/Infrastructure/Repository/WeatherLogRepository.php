<?php

declare(strict_types=1);

namespace Weather\Infrastructure\Repository;

use DateTimeImmutable;
use Weather\Domain\Entity\WeatherLog;
use Weather\Domain\Model\Weather;
use Weather\Infrastructure\Enum\Station;

interface WeatherLogRepository
{
    public function fromStationByTime(Station $station, DateTimeImmutable $timestamp): ?WeatherLog;

    /* @return Weather[] */
    public function averageByDate(DateTimeImmutable $timestamp, Station ...$stations): array;

    public function lastImportDate(): ?DateTimeImmutable;

    public function create(WeatherLog $weather): void;
}
