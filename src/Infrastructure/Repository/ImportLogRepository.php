<?php

declare(strict_types=1);

namespace Weather\Infrastructure\Repository;

use Ramsey\Uuid\UuidInterface;
use Weather\Domain\Entity\ImportLog;
use Weather\Infrastructure\Enum\ImportStatus;

interface ImportLogRepository
{
    public function create(ImportLog $importLog): void;

    public function updateStatus(UuidInterface $uuid, ImportStatus $status): void;
}
