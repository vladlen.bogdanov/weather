<?php

declare(strict_types=1);

namespace Weather\Infrastructure\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self STARTED();
 * @method static self FINISHED();
 * @method static self FAILED();
 */
abstract class ImportStatus extends Enum
{
    public const STARTED = 'started';
    public const FINISHED = 'finished';
    public const FAILED = 'failed';
}
