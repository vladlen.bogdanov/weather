<?php

declare(strict_types=1);

namespace Weather\Infrastructure\Service;

use Psr\Log\LoggerInterface;
use Weather\Domain\Model\Weather;

final class WeatherNoFilter implements WeatherFilter
{
    public function __construct(
        private LoggerInterface $logger
    ) {
    }

    public function apply(Weather $weather): Weather
    {
        $this->logger->debug('Weather passed to filter', [
            'weather' => [
                'id' => $weather->id()->toString(),
                'station' => $weather->station()->getValue(),
                'timestamp' => $weather->timestamp()->getTimestamp(),
            ],
        ]);

        return $weather;
    }
}
