<?php

declare(strict_types=1);

namespace Weather\Infrastructure\Service;

use Weather\Domain\Model\Weather;

/*
 * should be used for conversions: km/h <-> mp/h, C <-> F, etc
 */
interface WeatherFilter
{
    public function apply(Weather $weather): Weather;
}
