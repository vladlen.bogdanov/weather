<?php

declare(strict_types=1);

namespace Weather\Infrastructure\Service;

use DateTimeImmutable;
use Weather\Domain\Entity\WeatherLog;
use Weather\Infrastructure\Enum\Station;

interface StationWeatherImporter
{
    public function station(): Station;

    /* @return WeatherLog[] */
    public function execute(DateTimeImmutable $dateToImport): array;
}
