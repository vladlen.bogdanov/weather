<?php

declare(strict_types=1);

namespace Weather\Application\Exception;

use Exception;
use Throwable;

final class WeatherImportFailed extends Exception
{
    public static function withPrevious(Throwable $previous): self
    {
        return new self('Weather import failed', 0, $previous);
    }
}
