<?php

declare(strict_types=1);

namespace Weather\Application\Service;

use DateTimeImmutable;
use Weather\Infrastructure\Enum\Station;

final class ReadStationAverageWeatherServiceContext
{
    private array $stations = [];

    public function __construct(
        private DateTimeImmutable $timestamp,
    ) {
    }

    public function addStation(Station $station): void
    {
        $this->stations[] = $station;
    }

    public function timestamp(): DateTimeImmutable
    {
        return $this->timestamp;
    }

    public function stations(): array
    {
        return $this->stations;
    }
}
