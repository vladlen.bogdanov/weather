<?php

declare(strict_types=1);

namespace Weather\Application\Service;

use Weather\Domain\Model\Weather;
use Weather\Infrastructure\Repository\WeatherLogRepository;
use Weather\Infrastructure\Service\WeatherFilter;

final class ReadStationAverageWeatherService
{
    public function __construct(
        private WeatherLogRepository $weatherLogRepository,
        private WeatherFilter $weatherFilter
    ) {
    }

    public function execute(ReadStationAverageWeatherServiceContext $context): array
    {
        $weatherArray = $this->weatherLogRepository->averageByDate(
            $context->timestamp(),
            ...$context->stations()
        );

        return array_map(
            function (Weather $weather): Weather {
                return $this->weatherFilter->apply($weather);
            },
            $weatherArray
        );
    }
}
