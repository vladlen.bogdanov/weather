<?php

declare(strict_types=1);

namespace Weather\Application\Service;

use DateTimeImmutable;
use Weather\Infrastructure\Repository\WeatherLogRepository;

final class ReadLatestImportedDateService
{
    public function __construct(
        private WeatherLogRepository $weatherLogRepository
    ) {
    }

    public function execute(): ?DateTimeImmutable
    {
        return $this->weatherLogRepository->lastImportDate();
    }
}
