<?php

declare(strict_types=1);

namespace Weather\Application\Service;

use Weather\Domain\Model\Weather;
use Weather\Infrastructure\Repository\WeatherLogRepository;
use Weather\Infrastructure\Service\WeatherFilter;

final class ReadStationWeatherService
{
    public function __construct(
        private WeatherLogRepository $weatherLogRepository,
        private WeatherFilter $weatherFilter
    ) {
    }

    public function execute(ReadStationWeatherServiceContext $context): ?Weather
    {
        $weatherLog = $this->weatherLogRepository->fromStationByTime(
            $context->station(),
            $context->timestamp()
        );

        if (null === $weatherLog) {
            return null;
        }

        return $this->weatherFilter->apply(
            Weather::fromEntity($weatherLog)
        );
    }
}
