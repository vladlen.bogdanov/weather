<?php

declare(strict_types=1);

namespace Weather\Application\Service;

use DateTimeImmutable;
use Psr\Log\LoggerInterface;
use Throwable;
use Weather\Application\Exception\WeatherImportFailed;
use Weather\Domain\Service\WeatherImporter;

final class ImportWeatherDataService
{
    public function __construct(
        private WeatherImporter $importer,
        private LoggerInterface $logger
    ) {
    }

    /**
     * @throws WeatherImportFailed
     */
    public function execute(DateTimeImmutable $importDate): void
    {
        $logContext = [
            'import_date' => $importDate,
        ];

        $this->logger->notice('Import started', $logContext);

        try {
            $this->importer->execute($importDate);
        } catch (Throwable $exception) {
            $this->logger->notice(
                'Import failed',
                array_merge($logContext, [
                    'error' => [
                        'message' => $exception->getMessage(),
                        'trace' => $exception->getTrace(),
                    ],
                ])
            );

            throw WeatherImportFailed::withPrevious($exception);
        }

        $this->logger->notice('Import finished', $logContext);
    }
}
