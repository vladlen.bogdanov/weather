<?php

declare(strict_types=1);

namespace Weather\Application\Service;

use DateTimeImmutable;
use Weather\Infrastructure\Enum\Station;

final class ReadStationWeatherServiceContext
{
    public function __construct(
        private Station $station,
        private DateTimeImmutable $timestamp
    ) {
    }

    public function station(): Station
    {
        return $this->station;
    }

    public function timestamp(): DateTimeImmutable
    {
        return $this->timestamp;
    }
}
