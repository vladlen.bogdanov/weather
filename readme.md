Entry point: bin/console

**Config**:
- app.yml - main config file, just includes others
- monolog.yml - default logger config to write logs to file and console
- parameters.yml - import configuration parameters such as dir and enabled stations
- services.yml - service configuration

**Import**

Working import is not implemented - abstraction is all I have so far.
Application calls import API through Application\Service\ImportWeatherDataService, 
then it calls Domain\Service\WeatherImporter which is configured as a chain of station handlers
excluding those that are not in "**enabled_stations**" parameter.

So to add a new station I just need to add new Enum value to StationEnum, 
create a new handler instance of StationWeatherImporter and enabled it in parameters.yml.

**Storage**

I have not implemented any storage here. Code is designed to choose any - all I need to do is to
add configuration and repository class. There is a stub repository just to make application work.




